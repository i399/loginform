﻿using AspLoginRegistrationForm.Models;
using AspLoginRegistrationForm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace AspLoginRegistrationForm.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult MyAccount()
        {
            if(Session["Email"]==null || Session["Password"] == null)
            {
                return Redirect("~/Home/Login");
            }
            return View();
        }
        public ActionResult Registration()
        {
            ViewBag.Message = "";
            return View();
        }

        [HttpPost]
        public ActionResult Registration(RegistrationModel registrationModel)
        {
            try
            {
                RepositoryService repositoryService = new RepositoryService();
                repositoryService.DatabaseCon("MyAccountForm");
                repositoryService.setData("Insert into Registration(FirstName,LastName,UserPassword,Email) values(" + "'" + registrationModel.FirstName + "'," + "'" + registrationModel.LastName + "'," + "'" + registrationModel.UserPassword + "'," + "'" + registrationModel.Email + "'" + ")");
                ViewBag.Message = "Registration Completed!!!";
            }
            catch(Exception e)
            {
                ViewBag.Message = e.Message;
            }
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            RepositoryService repositoryService = new RepositoryService();
            repositoryService.DatabaseCon("MyAccountForm");
            var data = repositoryService.getData("select Email,UserPassword from Registration where Email = " + "'" + loginModel.Email + "' and UserPassword = '" + loginModel.UserPassword + "'");
            bool isLogin = false;
            while (data.Read())
            {
                if(loginModel.Email.Equals(data[0].ToString()) && loginModel.UserPassword.Equals(data[1].ToString()))
                {
                    Session["Email"] = data[0].ToString();
                    Session["Password"] = data[1].ToString();
                    isLogin = true;
                    break;
                }
               
            }
            repositoryService.DatabaseCon("MyAccountForm").Close();
            ViewBag.Message = isLogin ? "Login Successfull!!!" : "Login Failed";

            if (isLogin)
            {
                return Redirect("~/Home/MyAccount");
            }

            return View();
        }
      
    }
}