﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspLoginRegistrationForm.Models
{
    public class LoginModel
    {
        public string UserPassword { set; get; }
        public string Email { set; get; }
    }
}