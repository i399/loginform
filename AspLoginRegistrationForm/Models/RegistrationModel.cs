﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspLoginRegistrationForm.Models
{
    public class RegistrationModel
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string UserPassword { set; get; }
        public string Email { set; get; }
    }
}