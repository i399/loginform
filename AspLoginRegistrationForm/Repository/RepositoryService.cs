﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AspLoginRegistrationForm.Repository
{
    public class RepositoryService
    {
        private String Connection = "";
        public SqlConnection connection;
        public SqlConnection DatabaseCon(String Connection)
        {

            this.Connection = Connection;

            connection = new SqlConnection("Server="+ServerInfo.ServerName+ ";Database=" + Connection + "; Integrated Security=true");

            connection.Open();

            return connection;
        }
        public void setData(String Query)
        {
            SqlConnection sc = new SqlConnection();
            SqlCommand com = new SqlCommand();
            sc.ConnectionString = ("Data Source="+ServerInfo.ServerName + ";Database=" + this.Connection + ";Integrated Security=True");
            sc.Open();
            com.Connection = sc;
            com.CommandText = (Query);
            com.ExecuteNonQuery();
            sc.Close();
        }

        public SqlDataReader getData(String Query)
        {
            SqlDataReader reading;

            SqlCommand comand = new SqlCommand(

            Query, connection);

            reading = comand.ExecuteReader();

            return reading;
        }
    }
}